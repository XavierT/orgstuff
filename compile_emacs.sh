#!/bin/bash
sudo apt purge -y emacs
cd /tmp
git clone https://github.com/emacs-mirror/emacs.git
cd emacs
sudo apt install autoconf make gcc texinfo libgtk-3-dev libxpm-dev libjpeg-dev libgif-dev libtiff5-dev libncurses5-dev libgnutls28-dev
./autogen.sh
./configure
make -j6
sudo make install