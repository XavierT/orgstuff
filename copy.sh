#!/bin/bash
# Installe la config de ORG
mkdir ~/.emacs.d/
cp init.el ~/.emacs.d/
#cp -R Modèles ~/
cp -R snippets ~/.emacs.d/
virtualenv venv --python=python3 && . ./venv/bin/activate && pip install git+https://github.com/XavierTolza/pyorg.git