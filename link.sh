#!/bin/bash
# Installe la config de ORG
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
mkdir $HOME/.emacs.d/
ln -s $DIR/init.el $HOME/.emacs.d/init.el
ln -s $DIR/snippets $HOME/.emacs.d/snippets
cd $HOME/.emacs.d
virtualenv venv --python=python3 && . ./venv/bin/activate && pip install git+https://github.com/XavierTolza/pyorg.git