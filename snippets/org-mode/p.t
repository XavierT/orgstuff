# -*- mode: snippet -*-
# name: python table
# key: .p.t
# --
#+begin_src python :results output table :session :exports both
from org import to_org

$1
print(to_torg(df))
#+end_src