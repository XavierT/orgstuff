# -*- mode: snippet -*-
# name: python input
# key: .p.i
# --
#+begin_src python :results value :session :exports input
$1
#+end_src